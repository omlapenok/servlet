package ru.edu;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class FinancialServlet extends HttpServlet {

    String result = "";
    String h1 = "";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getRequestURI();

        if ("/finance".equals(action)) {
            getServletContext().getRequestDispatcher("/calc.jsp").forward(req, resp);
        }else if ("/result".equals(action)) {
            req.setAttribute("result", result);
            req.setAttribute("h1", h1);
            getServletContext().getRequestDispatcher("/result.jsp").forward(req, resp);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        try {
            Double sum = Double.parseDouble(req.getParameter("sum"));
            Double rate = Double.parseDouble(req.getParameter("percentage"));
            Integer years = Integer.parseInt(req.getParameter("years"));

            if (sum <= 0 || rate <= 0 || years <= 0 ) throw new NumberFormatException();

            if( sum < 50000) {
                result = "Минимальная сумма на момент открытия вклада 50 000 рублей";
                h1 = "Ошибка";
            } else {
                h1 = "Результат";
                result = "Итоговая сумма " + String.valueOf(Math.round(sum * Math.pow(1 + rate / 100, years))) + " рублей";
            }

        }catch (NumberFormatException e){
            result = "Неверный формат данных. Скорректируйте значения";
            h1 = "Результат";
        }

        resp.sendRedirect("result");
    }

}
