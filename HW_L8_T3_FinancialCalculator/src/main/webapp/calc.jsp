<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title>FinancialServlet</title>
    <style>
    .form_container {
        width: 500px;
        margin-left: auto;
        margin-right: auto;

      }
    .field {
       padding: 10px 30px 10px 10px;
       text-align:right;
     }
     label {
        font-size: 18px;
        float:left;
     }
     .button input{
        font-size: 15px;
        font-weight: 500;
        background-color: #2d82e3;
        cursor: pointer;
        padding: 12px 28px;
        float:left;
     }
     input {
        background-color: #b3b1b1;
        text-align: center;
        font-size: 18px;
     }
    </style>
</head>
<body>
<div class="form_container">
    <h1>Калькулятор доходности вклада</h1>
    <form method="post" action="/result">
        <div class="field">
            <label>Сумма на момент открытия: </label>
            <input name="sum" value="100000">
        </div>
        <div class="field">
            <label>Процентная ставка: </label>
            <input name="percentage" value="10">
        </div>
        <div class="field">
            <label>Количество лет: </label>
            <input name="years" value="2">
        </div>
        <div class="button">
        <input type="submit" value="Посчитать">
        </div>
       </form>
</div>
</body>
</html>
