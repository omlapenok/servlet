package ru.edu;

public class UserInfo {

    private String lastname;
    private String name;
    private String patronymic;
    private String phone;
    private String hobby;
    private String bitbucket;

    public UserInfo(String lastname, String name, String patronymic, String phone, String hobby, String bitbucket) {

        this.lastname = lastname;
        this.name = name;
        this.patronymic = patronymic;
        this.phone = phone;
        this.hobby = hobby;
        this.bitbucket = bitbucket;
    }

    public String getLastname() {
        return lastname;
    }

    public String getName() {
        return name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public String getPhone() {
        return phone;
    }

    public String getHobby() {
        return hobby;
    }

    public String getBitbucket() {
        return bitbucket;
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "lastname='" + lastname + '\'' +
                ", name='" + name + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", phone='" + phone + '\'' +
                ", hobby='" + hobby + '\'' +
                ", bitbucket='" + bitbucket + '\'' +
                '}';
    }
}
