package ru.edu;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class MyServlet extends HttpServlet {

    private UserInfo user = new UserInfo("Lapenok","Olga", "Michailovna", "8-000-000-00-01", "learning java, salsa, yoga", "https://bitbucket.org/omlapenok");

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.setAttribute("user", user);
        getServletContext().getRequestDispatcher("/author.jsp").forward(req, resp);
    }
}
