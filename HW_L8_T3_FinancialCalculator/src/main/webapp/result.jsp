<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title>Result</title>
    <style>
        .form_container {
            width: 500px;
            margin-left: auto;
            margin-right: auto;

          }
         label {
            font-size: 18px;
         }
    </style>
</head>
<body>
<div class="form_container">
    <h1><% out.println(request.getAttribute("h1"));%></h1>
    <label> <% out.println(request.getAttribute("result"));%> </label>
</div>
</body>
</html>